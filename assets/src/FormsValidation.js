// loads the jquery package from node_modules
import $ from 'jquery';
import validate from "jquery-validation";

// Add jquery validation plugins to forms
$(document).ready(function() {
    $("form").validate();
});

// Add bootstrap tooltip
$(document).ready(function() {
    $("[data-bs-toggle='tooltip']").tooltip();
    $("[data-tooltip='tooltip']").tooltip();
});