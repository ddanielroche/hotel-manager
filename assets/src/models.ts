// Models Interfaces

export enum Status {
    Empty = 'E',
    Occupied = 'O'
}

export interface Room {
    id: number;
    name: string;
    maxGuests: number;
    floor: number;
    status: Status;
}

export interface Registry {
    id: number;
    guest: string;
    checkIn: Date;
    checkOut: Date;
    room: Room;
}

export interface RoomRegistry {
    room: Room;
    registry: Registry;
}

export interface ErrorsForm<T> {
    children: T;
    errors: Errors[];
}

export interface RegistryError {
    guest: Errors[];
    checkIn: Errors[];
    checkOut: Errors[];
    room: Errors[];
}

export interface Errors {
    cause: any;
    message: string;
}