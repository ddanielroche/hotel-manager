import $ from 'jquery';
import {Modal, Toast} from 'bootstrap';
import {CheckInOut} from "./CheckInOut";
import {RoomStatus} from "./RoomStatus";

/**
 * Get Room Status and execute check in/check out
 */
$(document).ready(function () {
    let checkInModalDiv = $('#checkInModal');
    if (checkInModalDiv.length) {
        let checkInModal = new Modal(checkInModalDiv, {keyboard: false}),
            registryForm = $('#registry_form'),
            modalButton = $('.check-in-out-button');

        let validator = registryForm.validate();
        checkInModalDiv.on('show.bs.modal', async function (event) {
            validator.resetForm();
            RoomStatus(event);
        });
        modalButton.on("click", () => {
            if (registryForm.valid()) {
                CheckInOut(checkInModal, validator);
            }
        })
    }

    // Show Boostrap Toast if message present in context
    let toastElList = [].slice.call(document.querySelectorAll('.toast'));
    toastElList.map(function (toastEl, index) {
        //data-bs-msg
        if (toastEl.getAttribute('data-bs-msg')) {
            return new Toast(toastEl, {
                delay: 4000 + index * 2000
            }).show();
        }
    })
});