/**
 * Check In - Check Out business logic
 * Update room status and view appearance
 */
import {ErrorsForm, Registry, RegistryError, Status} from "./models";
import { Toast} from 'bootstrap';

export const CheckInOut = (checkInModal, registryFormValidator) => {
    let formEl = document.forms['registry_form'],
        action = formEl.action,
        formData = new FormData(formEl),
        registryId: number = +formData.get('registry[id]');

    if (registryId) {
        action = action.toString().replace('checkin', `${registryId}/checkout`);
    }

    // Set Room Status in list view, according the change
    fetch(action, {
        method: formEl.method,
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        },// @ts-ignore
        body: new URLSearchParams(formData)
    }).then(response => {
        // If status code is 422, display validation errors.
        if (response.status === 422) {
            response.json().then(
                (data: ErrorsForm<RegistryError>) => {
                    let errors = {};
                    const attrNames = Object.keys(data.children);

                    attrNames.map(key => {
                        const errorList = data.children[key];

                        if (errorList.errors.length) {
                            errors[`registry[${key}]`] = errorList.errors[0].message;
                        }
                    })

                    if (data.errors.length > 0) {
                        errors["registry[id]"] =  data.errors[0].message;
                    }

                    registryFormValidator.showErrors(errors);
                }
            )
        } else { // Else, update room status.
            response.json().then(
                (data: Registry) => {
                    let roomStatus = document.querySelector('#room-status-' + data.room.id),
                        trRoom = document.querySelector('#tr-room-' + data.room.id),
                        toastEl = document.querySelector('#liveToast'),
                        element = toastEl.querySelector('#toast-body-l') as HTMLLabelElement

                    if (data.room.status === Status.Empty) {
                        roomStatus.classList.remove('fa-house-user');
                        roomStatus.classList.add('fa-house');

                        element.innerHTML = 'Checkout successfully'

                        trRoom.classList.remove('table-success');
                    } else {
                        roomStatus.classList.remove('fa-house');
                        roomStatus.classList.add('fa-house-user');

                        element.innerHTML = 'Checkin successfully'

                        trRoom.classList.add('table-success');
                    }
                    checkInModal.hide();

                    return new Toast(toastEl, {
                        delay: 4000
                    }).show();
                }
            );
        }
    });
};