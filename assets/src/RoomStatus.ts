import {RoomRegistry, Status} from "./models";

export const RoomStatus = (event) => {
    let button = event.relatedTarget,
        // Extract info from data-bs-* attributes,
        roomHref = button.getAttribute('data-bs-room-href'),
        modalBodyInput = document.querySelector('#registry_room') as HTMLInputElement;
    let roomId = button.getAttribute('data-bs-room-id');
    modalBodyInput.value = roomId;

    // Fetch room status
    fetch(roomHref, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    }).then(response => response.json()).then((data: RoomRegistry) => {
        const btnCheckInOut = document.querySelector('#labelCheckInOut') as HTMLLabelElement,
            registryGuest = document.querySelector('#registry_guest') as HTMLInputElement,
            registryCheckIn = document.querySelector('#registry_checkIn') as HTMLInputElement,
            registryCheckOut = document.querySelector('#registry_checkOut') as HTMLInputElement,
            registryId = document.querySelector('#registry_id') as HTMLInputElement,
            modalTitle = document.querySelector('.modal-title');

        // Set Values according to room state
        if (data.room.status === Status.Occupied) {
            modalTitle.textContent = 'Check Out Room: ' + roomId;
            registryId.value = data.registry.id.toString();
            registryGuest.value = data.registry.guest;
            registryCheckIn.value = data.registry.checkIn.toString().substring(0, 10);
            registryCheckOut.value = data.registry.checkOut.toString().substring(0, 10);
            btnCheckInOut.innerText = 'Check Out';
        } else {
            modalTitle.textContent = 'Check In Room: ' + roomId;
            registryId.value = null;
            registryGuest.value = '';
            registryCheckIn.value = new Date().toString().substring(0, 10);
            registryCheckOut.value = new Date().toString().substring(0, 10);
            btnCheckInOut.innerText = 'Check In';
        }
    });
}