#!/usr/bin/env bash
symfony composer install
symfony run yarn install
symfony run yarn dev
symfony console doctrine:migrations:migrate -n
symfony server:start --port=8000