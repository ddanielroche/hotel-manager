<?php

namespace App\Test\Controller;

use App\Entity\Hotel;
use App\Entity\Room;
use App\Enum\Status;
use App\Repository\HotelRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RoomControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private RoomRepository $repository;
    private HotelRepository $hotelRepository;
    private Hotel $hotelFixture;
    private string $path = '/hotels/%d/rooms';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Room::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }

        $this->hotelRepository = (static::getContainer()->get('doctrine'))->getRepository(Hotel::class);

        foreach ($this->hotelRepository->findAll() as $object) {
            $this->hotelRepository->remove($object, true);
        }

        $hotelFixture = new Hotel();
        $hotelFixture->setName('My Title');
        $hotelFixture->setStreet('My Title');
        $hotelFixture->setPostalCode('My Title');
        $hotelFixture->setCity('My Title');
        $hotelFixture->setCountry('My Title');
        $hotelFixture->setPhone('My Title');
        $hotelFixture->setEmail('My Title');

        $this->hotelRepository->add($hotelFixture, true);
        $this->hotelFixture = $hotelFixture;
        $this->path = sprintf($this->path, $this->hotelFixture->getId());
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Room index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testIndexFiltered(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Submit', [
            'search_room[name]' => 'a',
            'search_room[status]' => Status::Empty->value,
        ]);
        self::assertResponseStatusCodeSame(200);

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        //$this->markTestIncomplete();
        $this->client->request('GET', sprintf('%s/new', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'room[name]' => 'Testing',
            'room[maxGuests]' => '1',
            'room[floor]' => '1',
        ]);

        self::assertResponseRedirects($this->path . '?msg=1');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        //$this->markTestIncomplete();
        $fixture = new Room();
        $fixture->setName('My Title');
        $fixture->setMaxGuests(1);
        $fixture->setFloor(1);
        $fixture->setStatus(Status::Empty);
        $fixture->setHotel($this->hotelFixture);

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s/%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Room');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testStatus(): void
    {
        //$this->markTestIncomplete();
        $fixture = new Room();
        $fixture->setName('My Title');
        $fixture->setMaxGuests(1);
        $fixture->setFloor(1);
        $fixture->setStatus(Status::Empty);
        $fixture->setHotel($this->hotelFixture);

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s/%s/status', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        // Use assertions to check that the properties are properly displayed.
    }


    public function testStatusOccupied(): void
    {
        //$this->markTestIncomplete();
        $fixture = new Room();
        $fixture->setName('My Title');
        $fixture->setMaxGuests(1);
        $fixture->setFloor(1);
        $fixture->setStatus(Status::Occupied);
        $fixture->setHotel($this->hotelFixture);

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s/%s/status', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        //$this->markTestIncomplete();
        $fixture = new Room();
        $fixture->setName('My Title');
        $fixture->setMaxGuests(1);
        $fixture->setFloor(1);
        $fixture->setStatus(Status::Empty);
        $fixture->setHotel($this->hotelFixture);

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s/%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'room[name]' => 'Testing',
            'room[maxGuests]' => 2,
            'room[floor]' => 2,
        ]);

        self::assertResponseRedirects($this->path . '?msg=1');

        $fixture = $this->repository->findAll();

        self::assertSame('Testing', $fixture[0]->getName());
        self::assertSame(2, $fixture[0]->getMaxGuests());
        self::assertSame(2, $fixture[0]->getFloor());
    }

    public function testRemove(): void
    {
        //$this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Room();
        $fixture->setName('My Title');
        $fixture->setMaxGuests(1);
        $fixture->setFloor(1);
        $fixture->setStatus(Status::Empty);
        $fixture->setHotel($this->hotelFixture);

        $this->repository->add($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s/%d', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects($this->path . '?msg=1');
    }
}
