<?php

namespace App\Test\Controller;

use App\DataFixtures\AppFixtures;
use App\Entity\Hotel;
use App\Entity\Registry;
use App\Entity\Room;
use App\Enum\Status;
use App\Repository\RegistryRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistryControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private RegistryRepository $repository;
    private Room $roomFixture;
    private string $path = '/registry';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Registry::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }

        $hotelRepository = (static::getContainer()->get('doctrine'))->getRepository(Hotel::class);

        foreach ($hotelRepository->findAll() as $object) {
            $hotelRepository->remove($object, true);
        }

        $hotelFixture = new Hotel();
        $hotelFixture->setName('My Title');
        $hotelFixture->setStreet('My Title');
        $hotelFixture->setPostalCode('My Title');
        $hotelFixture->setCity('My Title');
        $hotelFixture->setCountry('My Title');
        $hotelFixture->setPhone('My Title');
        $hotelFixture->setEmail('My Title');

        $hotelRepository->add($hotelFixture, true);
        $this->hotelFixture = $hotelFixture;

        $roomRepository = (static::getContainer()->get('doctrine'))->getRepository(Room::class);

        foreach ($roomRepository->findAll() as $object) {
            $roomRepository->remove($object, true);
        }

        $roomFixture = new Room();
        $roomFixture->setName('My Title');
        $roomFixture->setMaxGuests(10);
        $roomFixture->setFloor(10);
        $roomFixture->setStatus(Status::Empty);
        $roomFixture->setHotel($hotelFixture);

        $roomRepository->add($roomFixture, true);
        $this->roomFixture = $roomFixture;
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Registry index');
    }

    public function testIndexRoom(): void
    {
        $crawler = $this->client->request('GET', $this->path, ['room' => $this->roomFixture->getId()]);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Registry index');
    }
}
