<?php

namespace App\Test\Controller;

use App\Entity\Hotel;
use App\Repository\HotelRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HotelControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private HotelRepository $repository;
    private string $path = '/hotels';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Hotel::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Hotel index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        //$this->markTestIncomplete();
        $this->client->request('GET', sprintf('%s/new', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'hotel[name]' => 'Testing',
            'hotel[street]' => 'Testing',
            'hotel[postalCode]' => 'Testing',
            'hotel[city]' => 'Testing',
            'hotel[country]' => 'ES',
            'hotel[phone]' => 'Testing',
            'hotel[email]' => 'abc@example.com',
        ]);

        self::assertResponseRedirects('/hotels?msg=1');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        //$this->markTestIncomplete();
        $fixture = new Hotel();
        $fixture->setName('My Title');
        $fixture->setStreet('My Title');
        $fixture->setPostalCode('My Title');
        $fixture->setCity('My Title');
        $fixture->setCountry('My Title');
        $fixture->setPhone('My Title');
        $fixture->setEmail('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s/%d', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Hotel');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        //$this->markTestIncomplete();
        $fixture = new Hotel();
        $fixture->setName('My Title');
        $fixture->setStreet('My Title');
        $fixture->setPostalCode('08220');
        $fixture->setCity('My Title');
        $fixture->setCountry('ES');
        $fixture->setPhone('My Title');
        $fixture->setEmail('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s/%d/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'hotel[name]' => 'Something New',
            'hotel[street]' => 'Something New',
            'hotel[postalCode]' => '08220',
            'hotel[city]' => 'Something New',
            'hotel[country]' => 'ES',
            'hotel[phone]' => 'Something New',
            'hotel[email]' => 'abc@example.com',
        ]);

        self::assertResponseRedirects('/hotels?msg=1');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getName());
        self::assertSame('Something New', $fixture[0]->getStreet());
        self::assertSame('08220', $fixture[0]->getPostalCode());
        self::assertSame('Something New', $fixture[0]->getCity());
        self::assertSame('ES', $fixture[0]->getCountry());
        self::assertSame('Something New', $fixture[0]->getPhone());
        self::assertSame('abc@example.com', $fixture[0]->getEmail());
    }

    public function testRemove(): void
    {
        //$this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Hotel();
        $fixture->setName('My Title');
        $fixture->setStreet('My Title');
        $fixture->setPostalCode('My Title');
        $fixture->setCity('My Title');
        $fixture->setCountry('My Title');
        $fixture->setPhone('My Title');
        $fixture->setEmail('My Title');

        $this->repository->add($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s/%d', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/hotels?msg=1');
    }
}
