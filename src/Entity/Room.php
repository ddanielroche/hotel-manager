<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Enum\Status;
use App\Repository\RoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RoomRepository::class)]
#[ApiResource(
    paginationEnabled: true,
)]
#[ApiFilter(SearchFilter::class, properties: ['hotel' => 'exact', 'name' => 'partial', 'status' => 'exact'])]
class Room
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['room:list', 'room:item'])]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 50)]
    #[Groups(['room:list', 'room:item'])]
    private ?string $name = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['room:list', 'room:item'])]
    private ?int $maxGuests = null;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['room:list', 'room:item'])]
    private ?int $floor = null;

    #[ORM\ManyToOne(inversedBy: 'rooms')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    private ?Hotel $hotel = null;

    #[ORM\OneToMany(mappedBy: 'room', targetEntity: Registry::class, orphanRemoval: true)]
    private Collection $registrations;

    #[ORM\Column(type: 'string', length: 1, enumType: Status::class, options: ['default' => Status::Empty])]
    #[Groups(['room:list', 'room:item'])]
    #[Assert\NotBlank]
    private ?Status $status = Status::Empty;

    public function __construct()
    {
        $this->registrations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMaxGuests(): ?int
    {
        return $this->maxGuests;
    }

    public function setMaxGuests(int $maxGuests): self
    {
        $this->maxGuests = $maxGuests;

        return $this;
    }

    public function getFloor(): ?int
    {
        return $this->floor;
    }

    public function setFloor(int $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getHotel(): ?Hotel
    {
        return $this->hotel;
    }

    public function setHotel(?Hotel $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
