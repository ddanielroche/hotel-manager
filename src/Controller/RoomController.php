<?php

namespace App\Controller;

use App\Entity\Hotel;
use App\Entity\Room;
use App\Entity\RoomSearch;
use App\Enum\Status;
use App\Form\RegistryType;
use App\Form\RoomType;
use App\Form\SearchRoomType;
use App\Repository\RegistryRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/hotels/{hotel}/rooms')]
class RoomController extends AbstractController
{
    #[Route('', name: 'app_room_index', methods: ['GET', 'POST'])]
    public function index(Request $request, RoomRepository $roomRepository, Hotel $hotel): Response
    {
        $room = new RoomSearch();
        $form = $this->createForm(SearchRoomType::class, $room);
        $form->handleRequest($request);

        $registryForm = $this->createForm(RegistryType::class, null, [
            'action' => $this->generateUrl('app_registry_checkin'),
            'attr' => [
                'id' => 'registry_form'
            ],
        ]);

        return $this->render('room/index.html.twig', [
            'rooms' => $roomRepository->findByHotel($hotel, $room),
            'hotel' => $hotel,
            'search_room_form' => $form->createView(),
            'registry_form' => $registryForm->createView(),
        ]);
    }

    #[Route('/new', name: 'app_room_new', methods: ['GET', 'POST'])]
    public function new(Request $request, RoomRepository $roomRepository, Hotel $hotel, NotifierInterface $notifier): Response
    {
        $room = new Room();
        $room->setHotel($hotel);
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $roomRepository->add($room, true);

            $notifier->send(new Notification('Room added successfully', ['browser']));
            return $this->redirectToRoute('app_room_index', ['hotel' => $room->getHotel()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('room/new.html.twig', [
            'room' => $room,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_room_show', methods: ['GET'])]
    public function show(Room $room): Response
    {
        return $this->render('room/show.html.twig', [
            'room' => $room,
        ]);
    }

    #[Route('/{id}/status', name: 'app_room_status', methods: ['GET'])]
    public function status(Room $room, RegistryRepository $registryRepository): Response
    {
        $occupied = $room->getStatus() == Status::Occupied;
        return $this->json([
            'room' => $room,
            'registry' => $occupied ? $registryRepository->findLastGuestRegistered($room) : null,
        ], Response::HTTP_OK, [], [
            'groups' => ['room:item', 'registry:item']
        ]);
    }

    #[Route('/{id}/edit', name: 'app_room_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Room $room, RoomRepository $roomRepository, NotifierInterface $notifier): Response
    {
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $roomRepository->add($room, true);

            $notifier->send(new Notification('Room updated successfully', ['browser']));
            return $this->redirectToRoute('app_room_index', ['hotel' => $room->getHotel()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('room/edit.html.twig', [
            'room' => $room,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_room_delete', methods: ['POST'])]
    public function delete(Request $request, Room $room, RoomRepository $roomRepository, NotifierInterface $notifier): Response
    {
        if ($this->isCsrfTokenValid('delete'.$room->getId(), $request->request->get('_token'))) {
            $roomRepository->remove($room, true);

            $notifier->send(new Notification('Room deleted successfully', ['browser']));
        }

        return $this->redirectToRoute('app_room_index', ['hotel' => $room->getHotel()->getId()], Response::HTTP_SEE_OTHER);
    }
}
