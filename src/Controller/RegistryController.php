<?php

namespace App\Controller;

use App\Entity\Registry;
use App\Enum\Status;
use App\Form\RegistryType;
use App\Repository\RegistryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/registry')]
class RegistryController extends AbstractController
{
    #[Route('', name: 'app_registry_index', methods: ['GET'])]
    public function index(Request $request, RegistryRepository $registryRepository): Response
    {
        $room = $request->query->getInt('room');
        if ($room) {
            return $this->render('registry/index.html.twig', [
                'registries' => $registryRepository->findByRoom($room),
            ]);
        }
        return $this->render('registry/index.html.twig', [
            'registries' => $registryRepository->findBy([], ['id' => 'ASC']),
        ]);
    }

    #[Route('/checkin', name: 'app_registry_checkin', methods: ['GET', 'POST'])]
    public function new(Request $request, RegistryRepository $registryRepository): Response
    {
        $registry = new Registry();
        $form = $this->createForm(RegistryType::class, $registry, ['allow_extra_fields' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $registry->getRoom()->setStatus(Status::Occupied);

            $registryRepository->add($registry, true);

            return $this->json($registry, 200, [], [
                'groups' => ['room:item', 'registry:item']
            ]);
        }

        return $this->json($form, 422);
    }

    #[Route('/{id}/checkout', name: 'app_registry_checkout', methods: ['GET', 'POST'])]
    public function edit(Request $request, Registry $registry, RegistryRepository $registryRepository): Response
    {
        $form = $this->createForm(RegistryType::class, $registry, ['allow_extra_fields' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $registry->getRoom()->setStatus(Status::Empty);

            $registryRepository->add($registry, true);

            return $this->json($registry, 200, [], [
                'groups' => ['room:item', 'registry:item']
            ]);
        }

        return $this->json($form, 422);
    }
}
