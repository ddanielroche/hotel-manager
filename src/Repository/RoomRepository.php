<?php

namespace App\Repository;

use App\Entity\Hotel;
use App\Entity\Room;
use App\Entity\RoomSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Room>
 *
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Room::class);
    }

    public function add(Room $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Room $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Room[] Returns an array of Room objects
     */
    public function findByHotel(Hotel $hotel, RoomSearch $room): array
    {
        $queryBuilder = $this->createQueryBuilder('r')->andWhere('r.hotel = :hotel')->setParameter('hotel', $hotel);

        if ($room->getName() != null) {
            $queryBuilder->andWhere('LOWER(r.name) LIKE :name')->setParameter('name', strtolower("%{$room->getName()}%"));
        }

        if ($room->getStatus() != null) {
            $queryBuilder->andWhere('r.status = :status')->setParameter('status', $room->getStatus());
        }

        return $queryBuilder->orderBy('r.id' , 'ASC')->getQuery()->getResult();
    }
}
