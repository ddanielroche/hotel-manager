<?php

namespace App\Repository;

use App\Entity\CheckOut;
use App\Entity\Registry;
use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Registry>
 *
 * @method Registry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Registry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Registry[]    findAll()
 * @method Registry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Registry::class);
    }

    public function add(Registry $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Registry $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findLastGuestRegistered(Room $room): ?Registry
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.room = :val')
            ->setParameter('val', $room)
            ->orderBy('r.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return Registry[] Returns an array of Registry objects
     */
    public function findByRoom(int $room): array
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.room = :val')
            ->setParameter('val', $room)
            ->orderBy('r.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}