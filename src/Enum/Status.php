<?php

namespace App\Enum;

enum Status: string
{
    case Empty = 'E';
    case Occupied = 'O';
}