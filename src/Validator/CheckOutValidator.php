<?php

namespace App\Validator;

use App\Entity\Registry;
use App\Enum\Status;
use DateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class CheckOutValidator extends ConstraintValidator
{

    public function validate(mixed $value, Constraint $constraint)
    {
        if (!$constraint instanceof CheckOut) {
            throw new UnexpectedTypeException($constraint, CheckOut::class);
        }

        if (!$value instanceof Registry) {
            throw new UnexpectedValueException($value, Registry::class);
        }

        // Check Out validation if [Registry] exist
        if (!$value->getId()) {
            return;
        }

        $actual = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d 00:00:00'));
        // Check business constraints
        if ($value->getRoom()->getStatus() == Status::Empty) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', "The room is empty")
                ->addViolation();
        } elseif ($value->getCheckOut() < $value->getCheckIn()) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', 'The check out date not be less than the check in date')
                ->addViolation();
        }
    }
}