<?php

namespace App\Validator;

use App\Entity\Registry;
use App\Enum\Status;
use DateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class CheckInValidator extends ConstraintValidator
{

    public function validate(mixed $value, Constraint $constraint)
    {
        if (!$constraint instanceof CheckIn) {
            throw new UnexpectedTypeException($constraint, CheckIn::class);
        }

        if (!$value instanceof Registry) {
            throw new UnexpectedValueException($value, Registry::class);
        }

        // No Check In validation if [Registry] exist
        if ($value->getId()) {
            return;
        }

        $actual = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d 00:00:00'));
        // Check business constraints
        if ($value->getRoom()->getStatus() == Status::Occupied) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', "The room is occupied")
                ->addViolation();
        } elseif ($value->getCheckIn() < $actual) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', 'The check in date not be less than the current date')
                ->addViolation();
        } elseif ($value->getCheckOut() < $value->getCheckIn()) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', 'The check out date not be less than the check in date')
                ->addViolation();
        }
    }
}