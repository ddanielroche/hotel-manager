<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class CheckIn extends Constraint
{
    public string $message = '{{ string }}';

    public function getTargets(): array|string
    {
        return self::CLASS_CONSTRAINT;
    }
}